# zioup.com API

This is a toy API written to help me learn Rust.

A simplistic example of an with Rust iron.

I decided to open it publicly as I find there aren't enough examples of basic
web in Rust, one more can't hurt! Do keep in mind that I'm still learning Rust
and this might not be ideal/correct.

The latest version should be running at https://api.zioup.com/

