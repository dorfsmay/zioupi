extern crate iron;
extern crate router;
extern crate cookie;

use iron::{Iron, Request, Response, IronResult, AfterMiddleware, Chain};
use iron::error::IronError;
use iron::status;
use router::{Router, NoRoute};


struct Custom404;
impl AfterMiddleware for Custom404 {
    fn catch(&self, _: &mut Request, err: IronError) -> IronResult<Response> {
        if let Some(_) = err.error.downcast::<NoRoute>() {
            Ok(Response::with((status::NotFound, "¯\\_(ツ)_/¯\n")))
        } else {
            Err(err)
        }
    }
}

struct DefaultContentType;
impl AfterMiddleware for DefaultContentType {
    fn after(&self, _req: &mut Request, mut resp: Response) -> IronResult<Response> {
        if resp.headers.get::<iron::headers::ContentType>() == None {
            resp.headers.set(iron::headers::ContentType::plaintext());
        }
        Ok(resp)
    }
}


struct BeenHereCookie;
impl AfterMiddleware for BeenHereCookie {
    fn after(&self, req: &mut Request, mut resp: Response) -> IronResult<Response> {
        // #TODO: This needs some serious refactoring!
        let been_there_result = match req.headers.get::<iron::headers::Cookie>() {
            Some(cookie_header) => {
                let mut temp_result = Err(());
                for cookie_raw in cookie_header.iter() {
                    if let Ok(cookie) = cookie_raw.parse::<cookie::Cookie>() {
                        if cookie.name() == "BeenThere" {
                            temp_result = match cookie.value().parse::<u64>() {
                                Ok(this) => Ok(this),
                                Err(_e) => Err(()),
                            };
                            break;
                        }
                    }
                }
                temp_result
            }
            None => Err(()),
        };
        let been_there_count: u64 = match been_there_result {
            Ok(x) => x + 1,
            Err(_e) => 0,
        };

        let mut set_cookie_header = match resp.headers.get::<iron::headers::SetCookie>() {
            Some(h) => h.clone(),
            None => iron::headers::SetCookie(Vec::new()),
        };
        let cookie_string = format!("{}={}", "BeenThere", been_there_count);
        set_cookie_header.push(cookie_string.into());
        //let header = iron::headers::SetCookie(vec![cookie_string.into()]);
        resp.headers.set(set_cookie_header);
        Ok(resp)
    }
}


fn handler_ip(req: &mut Request) -> IronResult<Response> {
    // We assume X-Real-IP presence means there were proxies.
    let ip = match req.headers.get_raw("X-Real-IP") {
        Some(real_ip) => std::str::from_utf8(&real_ip[0]).unwrap().to_string(),
        None => format!("{}", req.remote_addr.ip()),
    };
    Ok(Response::with((status::Ok, format!("{}\n", ip))))
}

fn handler_headers(req: &mut Request) -> IronResult<Response> {
    Ok(Response::with((status::Ok, format!("{}", req.headers))))
}

fn handler_teapot(_req: &mut Request) -> IronResult<Response> {
    Ok(Response::with((status::ImATeapot, "⚫⚫⚫⚫\n".to_string())))
}

fn handler_no_name_cookie(_req: &mut Request) -> IronResult<Response> {
    let mut resp = Response::new();
    let header = iron::headers::SetCookie(vec!["Look_ma_no_name".into()]);
    resp.headers.set(header);
    resp.body = Some(Box::new("Setting cookie with value but no name. Check the Set-Cookie header.\n"));
    resp.status = Some(status::Ok);
    Ok(resp)
}

// https://is.gd/EUwRaE
fn handler_root(req: &mut Request) -> IronResult<Response> {
    let scheme = match req.headers.get_raw("X-Scheme") {
        //Some(xscheme) => std::str::from_utf8(&xscheme[0]).unwrap().to_string(),
        Some(xscheme) => std::str::from_utf8(&xscheme[0]).unwrap(),
        None => req.url.scheme(),
    };

    let url_string = format!("{}", req.url);
    let mut i = url_string.splitn(2, ':');
    let _ = i.next();
    let url = match i.next() {
        Some(lhs_url) => format!("{}:{}", scheme, lhs_url),
        None => url_string.to_string(),
    };


    let text = format!(r#"Hi... No doc yet... Try:

{url}headers
{url}noname
{url}ip
{url}source

Value of Set-Cookie BeenThere is incremented on each request within a session.

Come back soon!
"#,
                       url = url);
    Ok(Response::with((status::Ok, text)))
}

fn handler_source(_req: &mut Request) -> IronResult<Response> {
    let url = iron::Url::parse("https://gitlab.com/dorfsmay/zioupi/tree/master").unwrap();
    let redirect = iron::modifiers::Redirect(url);
    Ok(Response::with((status::Found, redirect)))
}


fn main() {
    let listening_port = 8080;
    let mut router = Router::new();

    router.get("/", handler_root, "root");
    router.get("/headers", handler_headers, "headers");
    router.get("/ip", handler_ip, "ip");
    router.get("/teapot", handler_teapot, "teapot");
    router.get("/nonamecookie", handler_no_name_cookie, "no_name_cookie");
    router.get("/source", handler_source, "source");

    let mut chain = Chain::new(router);
    chain.link_after(BeenHereCookie);
    chain.link_after(Custom404);
    chain.link_after(DefaultContentType);

    println!("Listening on {}", listening_port);
    Iron::new(chain)
        .http(format!("localhost:{}", listening_port))
        .unwrap();
}
