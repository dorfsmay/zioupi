
- add systemctl files

- add swagger like automatic documentation

- set cookie, eg: /setcookie?key=xxx&value=xx
  + PUT/

- return random number of dots for /teapot

- return json and html on everything based on path (eg: ip.json)

- set html automatic based on accepted content

- Add tests
    eg: non-numeric BeenThere cookie!

- Add /version based on Cargo.toml

- add a response time header (from github example)

